# Parallel computing exercise

Small step-by-step tutorial to perform parallel computation on the TIGON cluster at ICI - Centrale Nantes.  

The goal of this exercise is to show how a parallel computing system works/how to use it and to replicate Liger workflow. It is meant for beginners and expert users. It can be a useful exercise
for future LIGER/supercomputer users, to learn some aspects of HPC.

This exercise is followed by an evaluation survey, ideally to be done at the end of the exercise: [HERE](https://docs.google.com/forms/d/e/1FAIpQLSfutHY50tgt5_0OGqV44-WH6XDoqkSdPrVUoSlhuLYtGSeJow/viewform?usp=sf_link). 
**feel free to stop the exercise at any point but please take some time to fill the survey even if you are not done!**

![Tigon top view](tigon_top.jpg)*TIGON, the Raspberry PI + Lego cluster at ICI - Centrale Nantes*
## Intro
**For this tutorial it is better to be familiar with the command line tool (at least copy and paste!) and basic program execution. If you are already familiar with the 
LIGER environment and Slurm you can skip the introduction.**

Supercomputing (HPC) and parallel computation are used in a large variety of applications in scientific research and professional environments: heavy mathematical simulations such as
weather forecast or atomic reactions modelling and big data applications. The aim of parallel computing is to reduce the load and execution time of a given program by sharing the work
across multiple "workers" = processors, speeding everything up.

You are going to execute some programs on the TIGON cluster, a small super-computer. It is made exactly like a supercomputer (i.e. a lot of computers connected together) but on 
a much smaller scale. Below some terminology used in this tutorial:
- **node**: a single computer/device able to perform computations. A supercomputer system has several nodes connected together
- **core**: each node has a processor with a specific number of cores. Each core is able to perform operation indipendently. Therefore the number of cores corresponds to the
            number of simultaneous processes that can happen in that node. For example, a computer with a single-core processor can never execute more that one process at a time (sequential execution).
- **number of parallel processes**: it corresponds to the number of nodes * number of cores per node. The higher the number of parallel processes, the faster the super computer will be.

The "supercomputer" where you will run this exercise, the TIGON cluster, has a total of 6 nodes. Each node has a processor with 4 cores, giving a total of 24 maximum parallel processes.

## Create an account on TIGON
You have to create an account to be able to access the system. 

1.  Go to: [user registration](http://tigon.ec-nantes.fr:3000/user/clustercreate)
2.  Choose a username and a password following the instructions on the page.
3.  Click submit and check the output.

Now you should be able to log into the login node in the system and execute programs from your home folder `/sharedFS/scratch/<USERNAME>`. 
This folder has restricted access and is only for you to use.

## Log into the system with SSH
SSH is a program that allows the use of a remote computer from a terminal.
We are going to use SSH into the login node of the TIGON system. The login node is the entry point of the cluster, where we can run programs and manage the resources. 
 
Instruction changes depending whether you are using Windows or Linux/MacOS:

### Linux/MacOS
Open a terminal and run the following command, substituting <YOUR_USERNAME> with the username you registered with in the step above:
```
ssh <YOUR_USERNAME>@tigon.ec-nantes.fr
```
You will be prompted `<YOUR_USERNAME>@tigon.ec-nantes.fr's password:`. Insert the password you registered with in the step above and press enter. (N.B. even if you don't see anything 
being typed on the screen the password is there, it is just a security method).

### Windows

1.  Download PUTTy ([from here](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)) or any other SSH client. 
2.  Start the program once it has been downloaded.
3.  In the *Host Name or IP address* box type this `<YOUR_USERNAME>@tigon.ec-nantes.fr`, substituting <YOUR_USERNAME> with the username you registered with.
4.  Leave all other options unchanged and press the *Open* button.
5.  Enter your password. Please note that you will NOT see your cursor moving, or any characters typed (such as ******), when typing your password.


If you see a welcome message you are now logged in and ready to start the exercise. From now on, all comands need to be executed on this terminal.

## Clone this repository
You now have to copy the files for the exercise to your home folder in the raspberrHPC cluster. To do this we will clone this repository that constains all the necessary files.
Run the following command:
```
git clone https://gitlab.com/daviderovell0/parallel-computing-exercise/
```
The folder with the exercise files should now be on your home space. Go to the exercise directory by typing:
```
cd parallel-computing-exercise
```
If you now run ```ls``` you should see the following output:
```
<YOUR_USERNAME>@login01:~/parallel-computing-exercise $ ls
makefile  mpihello.sh  mpi_hello_world.c  README.md  seq_hello_world.c
```
## Run the program: parallel hello_world
*The hello world program was taken from www.mpitutorial.com, check the website if you want more insights on how the program actually works. The programming part 
is beyond the scope of this exercise.*

Now that we are logged into the system and have our programs ready to go, we need to be able to execute the program across multiple nodes. To do this we are going to use a 
batch scheduler called Slurm and an implementation of the Message Passing Interface(MPI), OpenMPI. This will allow the communication and coordination between the nodes that will
be able to work in parallel. If you are familiar with scheduler, batch schedulers and MPI you can skip the following section and go to Sequential execution.
### Parallel computing elements

##### Schedulers
Every program execution is governed by a scheduler. A single computer has an operating system scheduler that tells the processor's cores which processes to execute at 
one time and which ones to execute next. For example, when you start the internet browser on a commercial computer, the scheduler might automatically assign the task: "start the browser" to
2 cores in your processor, leaving the others free for other tasks. Note that one task (i.e. "start the browser") can be split into multiple cores. This is already an example of 
parallel computing called *threading* and it is automatically enabled by all the modern operating systems. 

##### Batch schedulers
In supercomputers the concept of scheduling is taken further. Since a supercomputer in its bare form is a group of computers (nodes) connected together, we need a scheduler that distributes
tasks across different nodes in the same way the operating system scheduler distributes tasks across cores in a single processor. 

Such a scheduler is called a **batch scheduler**. The TIGON cluster uses the Slurm batch scheduler, very common in supercomputer systems. A key difference between Slurm and 
a normal scheduler is that the user needs to decide how many resources (cores, nodes) to use for a given program. 

In this execise we will tell Slurm which program to execute, how many parallel processes we want to use and it will take care of assigning available resources and collecting the result.

##### Message Passing Interface (MPI)
Another requirement for the correct functioning of a parallel program is the communication between the different processes. If each process did its part of the program without communicating,
we would be left with a lot independent results without meaning. Imagine having a program that splits the following mathematical expressions into 3 different nodes: <br/>(2 + 2) * (4-1) * (7-5)<br/>
If each core computes an expression in round brackets without communicating, we would have the numbers 4, 3, 2 that tell us nothing about the final result.

The MPI communication protocol is used in distributed systems (i.e. supercomputers) for this purpose. The program needs to be written using this protocol for parallel computation.  <br/>
This subsection was just for information, you do not need to worry about using MPI. It is already implemented into the program.
### Sequential execution
We are going to start with sequential execution. This is the most standard way of writing and executing a program: if the code does not specifically 
use some parallel programming paradigm (such as MPI or threading as mentioned before), it will be executed sequentially. That is,
a single core will execute the entire program, one instruction at a time.

The sequential program is written in C and it's called *seq_hello_world.c*. It simply outputs the same message 10 times. The message contains a "hello world", the node that runs the program from 
and a time snapshot. This time snapshot represents the time in microseconds that has passed from a specific event in the past. It can be considered as a running stopwatch: each time the "hello
world" message is printed, the time is read from the stopwatch. The absolute value of the time does not matter, it has a meaning only if compared with another time value to see the order of 
execution.

We are going to use basic C compilation and execution commands. Run the following command to compile the code:
```
gcc seq_hello_world.c -o seq_hello_world
```
Now execute:
```
./seq_hello_world
```
You should see an output of 11 lines, starting with "Sequential execution:". If you see an error message check your syntax and your files with ```ls```.

If you look at the time snapshots (most likely last 3 digits), 
**Sequential execution: are the timestamps in increasing order? (note: timestamps could be positive or negative, check if there's minus at the beginning of the number) ->survey question** <br />
**Are you confident that each statement was executed after each other? ->survey question**
### Parallel execution
Next, we are going to use Slurm to compute the "hello world" program across different nodes in the cluster. The program outputs the same message as the sequential one, with the addition 
of the core which computed the statement. The time snapshot is calculated in the exact way as above.

Run the following (compilation is embedded):
```
sbatch -n10 ./mpihello.sh
```
You should see a message saying that a job was submitted in the system. The output is not directly sent to the console this time. Instead, Slurm wrote the output in a file called *output.txt*.
You can read the output with:
```
cat output.txt
```
You should see 10 lines starting with "Hello World". 

Look at the node names. **How many different node names do you see? ->survey question** <br/>
Each node in the TIGON cluster has 4 cores. The batch scheduler is set to assign each "hello world" to one core, that is why you should see at least 3 different nodes.

Look at the time snapshots. **Can you spot any particular order in the timings or do they appear random? ->survey question**

Re-run the commands as many times as you want to check different parallel executions. You can also increase the number of parallel processes by supplying a different ```-n``` flag. Example:
```
sbatch -n5 ./mpihello.sh 
```
for 5 parallel processes on 5 cores. Or:
```
sbatch -n15 ./mpihello.sh 
```
for 15 parallel processes on 15 cores. **Remember** the maximum number of parallel processes of this system is 24! Don't supply more than ```-n24``` processes, otherwise the program won't 
be executed and you might crash the system. 
## Conclusion
To summarise, we used a supercomputer system to run a parallel program that printed "Hello World" from different cores in different nodes in the system, demonstrating how we 
can use multiple resources at the same time. While the sequential exercise we could see incremental time snapshot, the random times obtained in the parallel execution proved how each "hello world"
message was executed indipendently in separate cores. Additionally, if we take the execution times *(optional exercise)* we will notice that the parallel execution is faster than the sequential one. <br/> 
On a normal supercomputer (such as LIGER), the programs are executed in the exact same way: through the batch scheduler. However the resources are much more, 
up to several hundreds of cores for a single program!
## Survey
Please take some time to do this short survey about this exercise: [HERE](https://docs.google.com/forms/d/e/1FAIpQLSfutHY50tgt5_0OGqV44-WH6XDoqkSdPrVUoSlhuLYtGSeJow/viewform?usp=sf_link)<br /> 
Answer the questions on the execution results and general opinions about the exercise and the TIGON system. 
## Optional (LIGER experienced users)
Feel free to experiment with the cluster, you can run any light program that you have run on LIGER on this system. **Be aware** that the resources are limited and it the execution 
times might be very long.  <br/>
The available softwares are:
- Compilers, interpreters: gcc, python 3.4
- MPI libraries for parallel computing in C/C++: OpenMPI, mpich 3.2
- HPC environment management: module environemt
- Binary Linear Algebra Subroutines (BLAS) implementations for calculus optimisation: ATLAS



