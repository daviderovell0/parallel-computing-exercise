#!/bin/bash
#SBATCH --output=output.txt

# Print the node that starts the process
echo "Master node: $(hostname)"

# Run our program using OpenMPI.
# OpenMPI will automatically discover resources from SLURM.
make --silent
mpirun ./mpi_hello_world