// Author: Wes Kendall
// Copyright 2011 www.mpitutorial.com
// This code is provided freely with the tutorials on mpitutorial.com. Feel
// free to modify it for your own use. Any distribution of the code must
// either provide a link to www.mpitutorial.com or keep this header intact.
//
// An intro MPI hello world program that uses MPI_Init, MPI_Comm_size,
// MPI_Comm_rank, MPI_Finalize, and MPI_Get_processor_name.
//

// Edited by: Davide Rovelli
// Part of parallel program exercise,
// gitLab: https://gitlab.com/daviderovell0/parallel-computing-exercise

#include <sys/time.h> 
#include <mpi.h>
#include <stdio.h>

long int readTime(struct timeval start) {
  struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return (currentTime.tv_sec * (int)1e6 + currentTime.tv_usec)-(start.tv_sec * (int)1e6 + start.tv_usec);
}

long int getMicrotime(){
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

int main(int argc, char** argv) {
  // Initialise the time variables
  struct timeval start;
  gettimeofday(&start,NULL); //"start" the stopwatch

  // Initialize the MPI environment. The two arguments to MPI Init are not
  // currently used by MPI implementations, but are there in case future
  // implementations might need the arguments.
  MPI_Init(NULL, NULL);

  // Get the number of processes
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  // Print off a hello world message
  printf("Hello world from node %s, core number: %d, out of %d cores. Time snapshot: %li microseconds\n",
         processor_name, world_rank, world_size,getMicrotime());

  // Finalize the MPI environment. No more MPI calls can be made after this
  MPI_Finalize();
}


