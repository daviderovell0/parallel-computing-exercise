// Author: Davide Rovelli
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

long int getMicrotime(){
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

int main(int argc, char** argv) {
    char hostname[1024];
    gethostname(hostname,1024);
    printf("Sequential execution:\n");
    for(int i = 0; i<10;i++){
        printf("Hello world from %s. Time snapshot: %li microseconds\n",hostname,getMicrotime());
    }
}
